const fs = require("fs");
const path = require("path");

const readJsonData = (fileName, slice) => {
  const filePath = path.join(process.cwd(), "data", `${fileName}.json`);
  const file = fs.readFileSync(filePath);
  const data = JSON.parse(file);
  return data;
};
module.exports = readJsonData;
